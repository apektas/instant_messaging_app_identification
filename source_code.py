import pandas as pd
import csv

# load data
# NOT: First extract zip file
data = pd.read_csv('all_nism_instant.csv',   quoting=csv.QUOTE_NONE, encoding='utf-8')

print(data.groupby('label').size())

# extact features and labels
# X feature set, y labels
X = data.loc[:,data.columns[:-1]] # -1 refers to label column
y = data.label
X = X.values

print(X.shape)

# Feature selection step.
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn import preprocessing


clf = ExtraTreesClassifier()
clf = clf.fit(X, y)
#print(clf.feature_importances_)

model = SelectFromModel(clf, prefit=True)
X = model.transform(X)
print(X.shape)

# train model and evalute the obtained model
from sklearn import linear_model, ensemble, svm, ensemble, tree, neural_network, metrics, naive_bayes
from sklearn import cross_validation

# cross validation of the ML algorithms
def stratified_cv(X, y, clf_class, shuffle=True, n_folds=10, **kwargs):
    stratified_k_fold = cross_validation.StratifiedKFold(y, n_folds=n_folds, shuffle=shuffle)
    y_pred = y.copy()
    for ii, jj in stratified_k_fold:
        X_train, X_test = X[ii], X[jj]
        y_train = y[ii]
        clf = clf_class(**kwargs)
        clf.fit(X_train,y_train)
        y_pred[jj] = clf.predict(X_test)
    return y_pred

# print accuracies of the ML algo.
print('Random Forest Classifier:       {:.2f}'.format(metrics.accuracy_score(y, stratified_cv(X, y, ensemble.RandomForestClassifier))))
print('Support vector machine(SVM):   {:.2f}'.format(metrics.accuracy_score(y, stratified_cv(X, y, svm.SVC))))
print('Logistic Regression:            {:.2f}'.format(metrics.accuracy_score(y, stratified_cv(X, y, linear_model.LogisticRegression))))